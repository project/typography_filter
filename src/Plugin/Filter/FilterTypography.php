<?php

namespace Drupal\typography_filter\Plugin\Filter;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use JoliTypo\Fixer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide the filter_typography text filter.
 *
 * @Filter(
 *   id = "filter_typography",
 *   title = @Translation("Improve typography"),
 *   description = @Translation("Use <a href=""https://github.com/jolicode/JoliTypo"">JoliTypo</a> to automatically fix typography by dealing with spaces around punctuation marks, replacing quotes, hyphenating, etc."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_IRREVERSIBLE,
 * )
 */
class FilterTypography extends FilterBase implements ContainerFactoryPluginInterface {

  /**
   * The module handle service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The theme manager service.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->moduleHandler = $container->get('module_handler');
    $instance->themeManager = $container->get('theme.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    $fixer = $this->initFixer($langcode);
    if ($fixer instanceof Fixer) {
      $text = $fixer->fix($text);
    }
    return new FilterProcessResult($text);
  }

  /**
   * Get the fixer according to the given langcode.
   *
   * Returns the fixer configured for the given langcode or NULL if the langcode
   * is not supported yet.
   *
   * @param string $langcode
   *   The language code of the text to be filtered.
   *
   * @return JoliTypo\Fixer|null
   *   The configured fixer.
   *
   * @see https://github.com/jolicode/JoliTypo#fixer-recommendations-by-locale
   * @todo make it configurable.
   */
  protected function initFixer($langcode) {
    $settings = [
      'fixers' => [],
      'locale' => '',
    ];

    switch ($langcode) {
      case 'en':
        $settings['fixers'] = [
          'Ellipsis',
          'Dimension',
          'Unit',
          'Dash',
          'SmartQuotes',
          'NoSpaceBeforeComma',
          'CurlyQuote',
          'Hyphen',
          'Trademark',
        ];
        $settings['locale'] = 'en_GB';
        break;

      case 'fr':
        $settings['fixers'] = [
          'Ellipsis',
          'Dimension',
          'Unit',
          'Dash',
          'SmartQuotes',
          'FrenchNoBreakSpace',
          'NoSpaceBeforeComma',
          'CurlyQuote',
          'Hyphen',
          'Trademark',
        ];
        $settings['locale'] = 'fr_FR';
        break;

      case 'de':
        $settings['fixers'] = [
          'Ellipsis',
          'Dimension',
          'Unit',
          'Dash',
          'SmartQuotes',
          'NoSpaceBeforeComma',
          'CurlyQuote',
          'Hyphen',
          'Trademark',
        ];
        $settings['locale'] = 'de_DE';
        break;

      default:
        return NULL;
    }

    $this->moduleHandler->alter('typography_filter_settings', $settings, $langcode);
    $this->themeManager->alter('typography_filter_settings', $settings, $langcode);

    $fixer = new Fixer($settings['fixers']);
    $fixer->setLocale($settings['locale']);
    return $fixer;
  }

}
