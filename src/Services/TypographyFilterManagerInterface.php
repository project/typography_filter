<?php

namespace Drupal\typography_filter\Services;

/**
 * Provides an interface defining the typography filter service.
 */
interface TypographyFilterManagerInterface {

  /**
   * List of all available rules.
   */
  const DEFAULT_RULES = [
    'CurlyQuote',
    'Dash',
    'Dimension',
    'Ellipsis',
    'FrenchNoBreakSpace',
    'Hyphen',
    'NoSpaceBeforeComma',
    'SmartQuotes',
    'Trademark',
    'Unit',
  ];

  /**
   * Process HTML content with fixer's rules.
   *
   * @param string $content
   *   HTML content need to be processed.
   * @param array $rules
   *   Array of rules to apply. By default, all available rules applies.
   * @param string $locale
   *   Language code. By default, uses code of current language.
   * @param array $protected_tags
   *   Array of HTML tag names that the DOM parser must avoid.
   *   Nothing in those tags will be fixed.
   *
   * @return string
   *   Processed HTML content.
   */
  public function fix(string $content, array $rules = [], string $locale = '', array $protected_tags = []): string;

  /**
   * Process non HTML content with fixer's rules.
   *
   * @param string $content
   *   Content need to be processed.
   * @param array $rules
   *   Array of rules to apply. By default, all available rules applies.
   * @param string $locale
   *   Language code. By default, uses code of current language.
   *
   * @return string
   *   Processed content.
   */
  public function fixString(string $content, array $rules = [], string $locale = ''): string;

}
